var request = require('request'),
    path = require('path'),
    baseUrl = 'http://localhost:8080/geoserver/rest',
    dsGeostackXML = '<dataStore>' + 
        '<name>geostack</name>' +
        '<connectionParameters>' + 
            '<host>localhost</host>' + 
            '<port>5432</port>' + 
            '<database>geostack</database>' + 
            '<user>postgres</user>' + 
            '<dbtype>postgis</dbtype>' + 
        '</connectionParameters>' + 
    '</dataStore>';
    
function atomLink(relativePath) {
    return '<atom:link xmlns:atom="http://www.w3.org/2005/Atom" ' + 
        'rel="alternate" type="application/xml" ' + 
        'href="http://localhost:8080/geoserver/rest/' + relativePath + '" />';
} // atomLink
    
function createRequestData(url, body) {
    return {
        uri: url,
        method: 'POST',
        headers: {
            'Content-Type': 'text/xml'
        },
        body: body
    };
} // createRequestData
    
function initDatastore(workspace, callback) {
    var dsUrl = baseUrl + '/workspaces/' + workspace + '/datastores/geostack.json',
        reqData;
    
    // check to see if the workspace has been created
    request(signRequest({ url: dsUrl }), function(err, resp, body) {
        if (err || resp.statusCode !== 200) {
            reqData = signRequest(createRequestData(
                baseUrl + '/workspaces/' + workspace + '/datastores', 
                dsGeostackXML
            ));
            
            request(reqData, function(err, resp, body) {
                if (err || resp.statusCode !== 200) {
                    console.log('Unable to create datastore in the "' + workspace + '" workspace: ' + (err || body));
                } // if
                
                // fire the callback
                if (callback) {
                    callback();
                } // if..else
            });
        }
        else if (callback) {
            console.log('geostack datastore already exists in the ' + workspace + ' workspace, not creating.');
            callback();
        } // if..else
    });} // initDatastore

function initLayer(workspace, layer, callback) {
    var featureUrl = baseUrl + '/workspaces/' + workspace + '/datastores/geostack/featuretypes/' + layer + '.json',
        featureTypeXML = '<featureType>' + 
            '<name>' + layer + '</name>' + 
            '<nativeName>' + workspace + '_' + layer + '</nativeName>' + 
        '</featureType>';
    
    // check to see if the workspace has been created
    request(signRequest({ url: featureUrl }), function(err, resp, body) {
        if (err || resp.statusCode !== 200) {
            var reqData = signRequest(createRequestData(
                baseUrl + '/workspaces/' + workspace + '/datastores/geostack/featuretypes', 
                featureTypeXML
            ));
            
            request(reqData, function(err, resp, body) {
                if (err || resp.statusCode !== 201) {
                    console.log('Unable to create feature type "' + layer + '" in workspace "' + workspace + '": ' + body);
                } // if
                
                // fire the callback
                if (callback) {
                    callback();
                } // if..else
            });
        }
        else if (callback) {
            console.log('feature type "' + layer + '" exists in "' + workspace + '" workspace, no action required.');
            callback();
        } // if..else
    });
    
} // initLayer
    
function initWorkspace(workspace, callback) {
    var wsUrl = baseUrl + '/workspaces/' + workspace + '.json',
        reqData;
    
    // check to see if the workspace has been created
    request(signRequest({ url: wsUrl }), function(err, resp, body) {
        if (err || resp.statusCode !== 200) {
            reqData = signRequest(createRequestData(
                baseUrl + '/workspaces', 
                '<workspace><name>' + workspace + '</name></workspace>'
            ));
            
            request(reqData, function(err, resp, body) {
                if (err || resp.statusCode !== 200) {
                    console.log('Unable to create workspace "' + workspace + '": ' + (err || body));
                } // if
                
                if (callback) {
                    callback();
                } // if..else
            });
        }
        else if (callback) {
            console.log('workspace ' + workspace + ' already exists, not creating.');
            callback();
        } // if..else
    });
} // initWorkspace

function signRequest(requestData) {
    // initialise the auth header
    var authHeader = new Buffer('admin:geoserver').toString('base64');

    // create the headers if required
    if (! requestData.headers) {
        requestData.headers = {};
    } // if
    
    // add the authorization header
    requestData.headers.Authorization = 'Basic ' + authHeader;
    return requestData;
}

exports.check = function(callback) {
    request({ uri: baseUrl }, function(err, resp, body) {
        callback(err ? err : undefined);
    });
};

exports.run = function(dataPath, callback) {
    var dsParts = path.basename(dataPath).split('_'),
        workspace = dsParts[0],
        layerName = dsParts.slice(1).join('_');
    
    if (dsParts.length >= 2) {
        console.log('Initializing geoserver config for ' + workspace + ':' + layerName);
        
        // initialise the workspace
        initWorkspace(workspace, function() {
            initDatastore(workspace, function() {
                initLayer(workspace, layerName, callback);
            });
        });
    }
    else if (callback) {
        callback();
    } // if
};
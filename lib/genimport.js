var exec = require('child_process').exec,
    spawn = require('child_process').spawn,
    path = require('path'),
    fs = require('fs');

exports.check = function(callback) {
    exec('shp2pgsql', function(err, stdout, stderr) {
        callback(err ? err.message : undefined);
    });
};

exports.run = function(dataPath, callback) {
    console.log('creating PostGIS import for data in: ' + dataPath);
    
    var procArgs = [
            '-s',
            '4326',
            '-d',
            path.join(dataPath, 'shape', path.basename(dataPath) + '.dbf'),
            path.basename(dataPath)
        ],
        sqlOut = fs.createWriteStream(path.join(dataPath, 'import.sql'));
        
    sqlOut.on('open', function(fd) {
        // create the process
        proc = spawn('shp2pgsql', procArgs);
        
        // pipe standard out to the file
        proc.stdout.pipe(sqlOut);
        
        // when the process exists, fire the callback
        proc.on('exit', function(code) {
            // close the file
            // fs.close(fd);
            
            // fire the callback
            if (callback) {
                callback(code);
            } // if
        });
    }); 
};
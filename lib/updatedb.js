var spawn = require('child_process').spawn,
    path = require('path'),
    fs = require('fs');

exports.check = function(callback) {
    var output = '',
        proc = spawn('psql', [
            '--username=postgres',
            'geostack',
            '-c',
            ''
        ]);
        
    proc.on('exit', function(code) {
        callback(code ? 'Postgres not configured correctly, or geostack db not found' : undefined);
    });
};

exports.run = function(dataPath, callback) {
    console.log('importing db: ' + path.join(dataPath, 'import.sql'));
    
    var procArgs = [
            '--username=postgres',
            'geostack',
            '-f',
            path.join(dataPath, 'import.sql')
        ],
        proc = spawn('psql', procArgs);
        
    // when the process exists, fire the callback
    proc.on('exit', function(code) {
        // fire the callback
        if (callback) {
            callback(code);
        } // if
    });
};
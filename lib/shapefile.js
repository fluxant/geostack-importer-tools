var exec = require('child_process').exec,
    spawn = require('child_process').spawn,
    path = require('path');

exports.check = function(callback) {
    exec('ogr2ogr --help-general', function(err, stdout, stderr) {
        callback(err ? err.message : undefined);
    });
};

exports.run = function(dataPath, callback) {
    var shapeDir = path.join(dataPath, 'shape'),
        ogrArgs = [
            '-f',
            'ESRI Shapefile',
            path.join(dataPath, 'shape'),
            path.join(dataPath, 'index.vrt')
        ],
        proc;
        
    console.log('creating shapefile for data in: ' + dataPath);
    
    // if the shape directory already exists, then add the overwrite param also
    if (path.existsSync(shapeDir)) {
        ogrArgs.unshift('-overwrite');
    } // if
    
    // spawn the process
    proc = spawn('ogr2ogr', ogrArgs);
    
    // when the process exists, fire the callback
    proc.on('exit', function(code) {
        if (callback) {
            callback(code);
        } // if
    });
};
var fs = require('fs'),
    path = require('path'),
    toolchain = [
        require('./lib/shapefile'),
        require('./lib/genimport'),
        require('./lib/updatedb'),
        require('./lib/geoserver-import')
    ];
    
function checkTools(successCallback) {
    var errors = [],
        checksLeft = toolchain.length;
    
    // check each of the items in the toolchain
    for (var ii = 0; ii < toolchain.length; ii++) {
        toolchain[ii].check(function(checkErrors) {
            checksLeft--;
            
            // add the errors
            if (checkErrors) {
                errors.push(checkErrors);
            } // if
            
            // if we have no checks left, then check the errors
            if (checksLeft <= 0) {
                if (errors.length) {
                    throw new Error(errors.join('\n'));
                }
                else if (successCallback) {
                    successCallback();
                } // if..else
            }
        });
    } // for
} // checkTools
    
function runTools(dataPath) {
    
    var toolIdx = 0;
    
    function runCurrent() {
        if (toolIdx < toolchain.length) {
            toolchain[toolIdx].run(dataPath, function() {
                toolIdx++;
                runCurrent();
            });
        } // if
    } // runCurrent

    runCurrent();
} // runTools

// check the tools are installed and available
checkTools(function() {
    // find the directories that contain data
    fs.readdir('.', function(err, files) {
        // iterate through the files and see if we should process it
        for (var ii = 0; ii < files.length; ii++) {
            var dataPath = path.resolve(files[ii]),
                indexFile = path.join(dataPath, 'index.vrt');

            // if the index file exists then import
            if (path.existsSync(indexFile)) {
                runTools(dataPath);
            } // if
        } // for
    });
});
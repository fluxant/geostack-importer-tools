var debug = require('debug')('publish'),
    fs = require('fs'),
    path = require('path'),
    skipDirs = ['.git', 'node_modules'],
    db = require('nano')('http://localhost:5984').use('lbs'),
    neuron = require('neuron'),
    manager = new neuron.JobManager();
    //db = require('nano')('http://10.211.55.4:5984').use('lbs');
    
function indexItems(itemPath, dataset, callback) {
    
    debug('looking for poi items in: ' + itemPath);
    fs.readdir(itemPath, function(err, files) {
        (files || []).forEach(function(file) {
            manager.enqueue('readFile', dataset, path.join(itemPath, file));
        });
    });
} // indexItems

function findFiles(filename, cb) {
    fs.stat(filename, function(err, stats) {
        // if the file is a directory and not to be skipped, then process
        if (stats.isDirectory() && path.existsSync(filename + '/items')) {
            indexItems(path.join(filename, 'items'), path.basename(filename), function() {
                console.log('finished processing ' + filename + ' dataset');
                if (cb) {
                    cb();
                }
            });
        }
        else if (cb) {
            cb();
        }
    });
} // process

manager.addJob('readFile', {
    work: function(dataset, filename) {
        var job = this;
        
        // only process .json files
        if (path.extname(filename).toLowerCase() === '.json') {
            fs.readFile(filename, 'utf8', function(err, data) {
                if (! err) {
                    try {
                        manager.enqueue(
                            'pushUpdate', 
                            dataset.replace(/\_/g, ':') + ':' + path.basename(filename, '.json'), 
                            JSON.parse(data)
                        );
                    }
                    catch (e) {
                        debug('Invalid data in ' + filename, e);
                    } // try..catch
                }
                else {
                    debug('error reading file: ' + filename, err);
                }
                
                job.finished = true;
            });
        }
    }
});

manager.addJob('pushUpdate', {
    concurrency: 25,
    work: function(itemId, data) {
        var job = this;
        
        debug('looking for existing item: ' + itemId);
        db.get(itemId, function(err, resp) {
            if (! err) {
                data._rev = resp._rev;
            }
            
            debug('pushing update of item: ' + itemId);
            db.insert(data, itemId, function(err, resp) {
                if (err) {
                    debug('Error updating item: ' + itemId, err);
                    
                    // this request didn't work, so queue it up again
                    manager.enqueue('pushUpdate', itemId, data);
                }
                
                job.finished = true;
            });
        });
    }
});

// find the directories that contain data
debug('looking for poi definitions in: ' + process.cwd());
fs.readdir(process.cwd(), function(err, files) {
    (files || []).forEach(function(file) {
        debug('finding files in: ' + path.resolve(file));
        findFiles(path.resolve(file));
    });
});
var fs = require('fs'),
    path = require('path'),
    csv = require('ya-csv'),
    skipDirs = ['.git', 'node_modules'],
    
    // initialise the default format
    defaultFormat = {
        fields: [
            'id',
            'name',
            'lat',
            'lon'
        ],
        
        values: [
            'id', 
            'name',
            'pos.lat',
            'pos.lon'
        ]
    };
    
function extractData(data, formatValues) {
    var result = [];

    // iterate over the format fields
    for (var ii = 0; ii < formatValues.length; ii++) {
        var fieldValue;

        // if the format value is a function then process it
        if (typeof formatValues[ii] == 'function') {
            fieldValue = formatValues[ii].call(null, data);
        }
        else {
            var fields = formatValues[ii].split('.');

            // initialise the field data to the base data
            fieldValue = data;

            // iterate through the fields
            for (var fldIdx = 0; fldIdx < fields.length; fldIdx++) {
                fieldValue = fieldValue ? fieldValue[fields[fldIdx]] : '';
            } // for
        }

        result.push(fieldValue);
    } // for

    return result;
} // extractData
    
function generateIndex(filename, callback) {
    var format,
        index = csv.createCsvFileWriter(path.join(filename, 'index.csv'));
    
    // look for the csv format in the folder
    try {
        format = require(path.join(filename, '/csv-format')).format;
    }
    catch (e) {
        format = defaultFormat;
    } // try..catch
    
    console.log('GENERATING INDEX FOR: ' + filename);
    console.log(format);
    
    // initialise the summary column names
    index.writeRecord(format.fields);
    
    // read the files and generate the summary lines
    indexItems(path.join(filename, 'items'), format, index, function() {
        // fire the callbakc
        if (callback) {
            callback(filename);
        } // if
    });
} // generateIndex

function indexItems(itemPath, format, index, callback) {
    
    var processingCount;
    
    function readItem(filename) {
        // only process .json files
        if (path.extname(filename).toLowerCase() === '.json') {
            // read the file and process the data
            var data = fs.readFileSync(filename),
                parsedData,
                record;

            // decrement the processing count
            processingCount--;

            try {
                parsedData = JSON.parse(data);
            }
            catch (e) {
                console.log('Invalid data: ' + filename);
            } // try..catch
            
            if (parsedData) {
                // extract the valid data using the format
                record = extractData(parsedData, format.values);

                // write the data to the index
                index.writeRecord(record);
            } // if

            // if we have no items left to proces the fire the callback
            if (callback && processingCount <= 0) {
                callback();
            } // if
        } // if
    } // readItem
    
    fs.readdir(itemPath, function(err, files) {
        if (! err) {
            // update the processing count
            processingCount = files.length;

            // iterate through the files
            for (var ii = 0; ii < files.length; ii++) {
                readItem(path.join(itemPath, files[ii]));
            } // for
        }
        else if (callback) {
            callback();
        } // if..else
    });
} // indexItems

function process(filename) {
    fs.stat(filename, function(err, stats) {
        // if the file is a directory and not to be skipped, then process
        if (stats.isDirectory() && path.existsSync(filename + '/items')) {
            generateIndex(filename, function() {
                console.log('finished processing ' + filename + ' dataset');
            });
        } // if
    });
} // process

// find the directories that contain data
fs.readdir('.', function(err, files) {
    // iterate through the files and see if we should process it
    for (var ii = 0; ii < files.length; ii++) {
        process(path.resolve(files[ii]));
    } // for
});